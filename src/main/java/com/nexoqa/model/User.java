package com.nexoqa.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class User {

    private String name;
    private String lastName;
    private String address;
    private Integer age;
    private Integer phoneNumber;

}
