package com.nexoqa.service;

import com.nexoqa.model.Client;
import com.nexoqa.model.User;
import lombok.Getter;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Service
@Getter
public class SubscriberService {

    private String backendUrl = "http://localhost:8081";

    public void setBackendURL(String urlBase) {
        this.backendUrl = urlBase;
    }

    public ResponseEntity<Client> subscribeUser(User user) {
        HttpEntity<User> request = new HttpEntity<>(user);
        try {
            return new RestTemplate().exchange(getBackendUrl() + "/client-provider/client", HttpMethod.POST, request, Client.class);
        } catch (HttpClientErrorException httpException) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
    }

}
